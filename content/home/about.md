+++
# About/Biography widget.
widget = "about"
active = true
date = "2016-04-20T00:00:00"

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
	"Evolution of coronary circulation in sharks",
    "Comparative cardiovascular physiology",
	"Morphology of Shark Hearts",
    "Coronary circulation in fishes"
  ]

# List your qualifications (such as academic degrees).

[[education.courses]]
  course = "Postdoctoral Scholar"
  institution = "University of Guelph, Department of Integrative Biology"
  year = Current

[[education.courses]]
  course = "Postdoctoral Scholar"
  institution = "University of Pennsylvania, Perelmen School of Medicine, Department of Pharmacology"
  year = 2018

[[education.courses]]
  course = "Postdoctoral Scholar"
  institution = "University of Miami's Rosenstiel School of Marine and Atmospheric Science"
  year = 2017

[[education.courses]]
  course = "PhD in Zoology/Animal Biology"
  institution = "University of British Columbia"
  year = 2015

[[education.courses]]
  course = "M.Sc. in Comparative Physiology"
  institution = "University of British Columbia"
  year = 2010

[[education.courses]]
  course = "BSc in Biology"
  institution = "University of New Brunswick"
  year = 2006

+++

# Biography

Starting in Oct. 2017, I am a biomedical postdoctoral researcher at the University of Pennsylvania’s Perelman School of Medicine in Dr. Jeffrey Field’s lab. I completed my PhD with Dr. Anthony Farrell in the Zoology department at the University of British Columbia.

My primary over-arching interest is understanding how marine organisms that inhabit heterogeneous environments are able to physiologically cope with abiotic stressors and changing environments.


My current post doctoral research utilizes gene knockout and inducible gene knockout models to investigate how specific cell signaling pathways can lead to disease states, such as cardiomyopathy, cardiac conduction disease, and sudden cardiac death. Working with biomedical models has allowed me to expand my skillset to include genetic sequencing and these are valuable tools when considering the diverse responses of the cardiovascular system to environmental changes across, within, and between genders in ectothermic vertebrates.

My previous research was as a post-doctoral fellow in Dr. Martin Grosell’s lab at the University of Miami’s Rosenstiel School of Marine and Atmospheric Science. The research was part of the RECOVER consortium (Relationships of Effects of Cardiac Outcomes in fish for Validation of Ecological Risk). RECOVER is one of 12 research groups awarded grants to carry-out independent research following the April 20, 2010, Deepwater Horizon (DWH) oil spill.

[Read more here](post/biography-cont)
