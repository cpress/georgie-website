+++
# Date this page was created.
date = "2016-04-27"

# Project title.
title = "Cardiac Oxygen Supply and the Morphology of Shark Hearts"

# Project summary to display on homepage.
summary = "Providing novel insights into the evolution of the coronary artery and cardiac oxygen delivery in vertebrates. sharks are ideal model species as they are the most ancient vertebrates to possess a coronary artery."

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "shark.jpg"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["sharks","cardiac-function"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Optional featured image (relative to `static/img/` folder).
[header]
image = "headers/bubbles-wide.jpg"
caption = ""

+++

In this set of studies, I look at the the evolution of the cardiac oxygen supply using sharks as model system.

![](/test/img/shark.jpg)

By exploring inter and intra-species variation in cardiovascular variables, I found that changes in vascular resistance could overcome the compressive forces of ventricular contraction in such a way that allows coronary blood flow to increase with heart rate despite the consequent increase in vascular compression by the surrounding myocardial tissue.

By uncovering the mechanism which allows for the increased rate of oxygen delivery during elevated cardiac function I was able to detect similarities with mammalian cardiovascular function and identify a possible underlying mechanisms that may be phylogenetically conserved in vertebrates.

Furthermore, my morphological analysis of the coronary circulation across shark taxa using classical histological techniques combined with phylogenetic analysis and basic fluid dynamics led me to discover that temperature plays a role as a selective pressure on the design of vascular beds. This work provided novel insights into our understanding of the evolution of the coronary artery and cardiac oxygen delivery in vertebrates.
