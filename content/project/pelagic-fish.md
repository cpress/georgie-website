+++
# Date this page was created.
date = "2016-04-27"

# Project title.
title = "Cardiac function following oil exposure in pelagic fish"

# Project summary to display on homepage.
summary = "During my postdoc as part of the RECOVER consortium, funded through GOMRI,  I have investigated whether phenotypic plasticity allows species to overcome anthropogenic sources of environmental pollution in the form of polycyclic aromatic hydrocarbons (PAH’s), the toxic components of oil."

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "pres-fish.jpg"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["oil","cardiac-function"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Optional featured image (relative to `static/img/` folder).
[header]
image = "headers/bubbles-wide.jpg"
caption = ""

+++


During my postdoc as part of the RECOVER consortium, funded through GOMRI,  I have investigated whether phenotypic plasticity allows species to overcome anthropogenic sources of environmental pollution in the form of polycyclic aromatic hydrocarbons (PAH’s), the toxic components of oil. 


<img src="/img/pres-fish.jpg" align="right" width="50%">

In this study I used a perfused heart technique to investigate the compensatory capacity of the cardiovascular system to overcome the reduced contractile function that results from acute PAH exposure in pelagic fish species. 

I discovered an interactive effect between PAH exposure and adrenergic stimulation that suggested catecholamines could play a compensatory role that may help to mitigate adverse effects of PAH exposure on the heart _in vivo_. 

My results indicated adrenergic nerve stimulation in combination with increased circulating catecholamines might be able to rescue cardiac function for brief periods in exposed individuals. However, a proportion of PAH exposed fish were unable to recover from maximal exertion despite elevated catecholamine levels. 

This research was the first to demonstrate that acute exposure to environmentally relevant concentrations of oil has profound effects on cardiovascular function at moderate to high levels of cardiac activity in athletic pelagic fish species. This research was presented locally for the general public, and nationally and internationally for the scientific community. 
